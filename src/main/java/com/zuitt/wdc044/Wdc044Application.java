package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Wdc044Application {

	public static void main(String[] args) {

		SpringApplication.run(Wdc044Application.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value= "name", defaultValue = "World") String name){
		return String.format("Hello %s", name);
	}
	@GetMapping("/greetings")
	public String greetings(@RequestParam(value= "greet", defaultValue = "World") String name){
		return String.format("Good evening, %s Welcome to batch 265", name);
	}
	@GetMapping("/contactInfo")
	public String contactInfo(@RequestParam(value = "name", defaultValue = "User") String name, @RequestParam(value="email", defaultValue = "user.email.com") String email){
		return String.format("Hello %s! Your email is %s", name, email);
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value= "user", defaultValue = "user") String user){
		return String.format("hi, %s!", user);
	}
	@GetMapping("/nameAge")
	public String nameAge(@RequestParam(value = "name", defaultValue = "User") String name, @RequestParam(value="age", defaultValue = "0") String age){
		return String.format("Hello %s! Your age is %s", name, age);
	}


}
